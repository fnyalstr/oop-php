<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "Name = " . $sheep->name . "<br>"; // "shaun"
echo "legs = " . $sheep->legs . "<br>"; // 4
echo "cold blooded = " . $sheep->cold_blooded . "<br><br>"; // "no"

$buduk = new Frog("buduk");
echo "Name = " . $buduk->name . "<br>"; // "buduk"
echo "legs = " . $buduk->legs . "<br>"; // 4
echo "cold blooded = " . $buduk->cold_blooded . "<br>"; // "no"
echo $buduk->Jump("Hop Hop") . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name = " . $sungokong->name . "<br>"; // "sungokong"
echo "legs = " . $sungokong->legs . "<br>"; // 2
echo "cold blooded = " . $sungokong->cold_blooded . "<br>"; // "no"
echo $sungokong->yell("Auooo"); // "Auooo"
?>